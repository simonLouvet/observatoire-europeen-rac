import vue from 'html-loader!./vue.html';
import showdown from 'showdown';
import group from '../Group/model.js';
import member from '../Member/model.js';
import nationalGroup from '../NationalGroup/model.js';

export default class LawDetail extends HTMLElement {
  constructor() {
    super();
    this.subscriptions = [];
    this.attachShadow({
      mode: 'open'
    });
    this.shadowRoot.innerHTML = vue;
  }
  connectedCallback() {

    this.propagatedStyle = this.querySelectorAll('style');
    this.propagatedStyle.forEach(style => {
      let injectedStyle = document.createElement('style');
      injectedStyle.appendChild(document.createTextNode(style.innerText));
      this.shadowRoot.appendChild(injectedStyle)
    })

  }

  disconnectedCallback() {
    // console.log('disconnectedCallback', this.subscriptions);
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    });
  }

  attributeChangedCallback(attrName, oldVal, newVal) {}

  setChannel(chan, postal) {
    this.channel = chan;
    this.postal = postal;
    this.subscriptions.push(this.channel.subscribe('law-changed', (law) => {
      this.setData(law);
    }));
    this.subscriptions.push(this.channel.subscribe('route-changed', (route) => {
      if (route.screen == 'law-detail') {
        this.channel.publish('law-load', route.id);
      }
    }))
    this.subscriptions.push(this.channel.subscribe('group-select', (group) => {
      console.log('group-select',group);
      let voteContainer= this.shadowRoot.querySelector('#voteContainer');

      while (voteContainer.firstChild != null) {
        voteContainer.removeChild(voteContainer.firstChild);
      }
      voteContainer.classList.remove('hide');

      group.nationalGroups.forEach(v => {
        let nationalGroupWC = document.createElement('national-group-wc');
        nationalGroupWC.setAttribute('mode','square')
        for (let style of this.propagatedStyle) {
          let injectedTemplate = document.createElement('template');
          let injectedStyle = document.createElement('style');
          injectedStyle.appendChild(document.createTextNode(style.innerText));
          injectedTemplate.appendChild(injectedStyle);
          nationalGroupWC.appendChild(injectedTemplate)
        }
        voteContainer.appendChild(nationalGroupWC);
        nationalGroupWC.setData(v);
      })
      let groups= this.shadowRoot.querySelector('#groups');
      groups.scrollIntoView(true);
    }))

  }

  renderLaw() {

  }

  setData(law) {
    console.log("lawDetail setData",law);

    var converter = new showdown.Converter({ openLinksInNewWindow: true });
    var html = converter.makeHtml(law.md);
    this.shadowRoot.querySelector('#text').insertAdjacentHTML('afterbegin', html);
    // console.log(group);
    law.groups.forEach((group, i) => {
      let groupWC = document.createElement('group-container');
      // console.log(groupWC);
      for (let style of this.propagatedStyle) {
        let injectedTemplate = document.createElement('template');
        let injectedStyle = document.createElement('style');
        injectedStyle.appendChild(document.createTextNode(style.innerText));
        injectedTemplate.appendChild(injectedStyle)
        groupWC.appendChild(injectedTemplate)
      }
      groupWC.setChannel(this.channel);
      let groupContainer = this.shadowRoot.querySelector('#groups');
      groupContainer.appendChild(groupWC);

      groupWC.setData(group);
    })
  }
}
window.customElements.define('law-detail-wc', LawDetail);
