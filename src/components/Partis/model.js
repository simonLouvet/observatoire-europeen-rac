import vue from 'html-loader!./vue.html';
import nationalGroup from '../NationalGroup/model.js';
import theme from '../Theme/model.js';
import group from '../Group/model.js';

export default class Partis extends HTMLElement {
  constructor() {
    super();
    this.subscriptions = [];
    this.attachShadow({
      mode: 'open'
    });
    this.shadowRoot.innerHTML = vue;
  }
  connectedCallback() {
    this.propagatedStyle = this.querySelectorAll('style');
    this.propagatedStyle.forEach(style => {
      let injectedStyle = document.createElement('style');
      injectedStyle.appendChild(document.createTextNode(style.innerText));
      this.shadowRoot.appendChild(injectedStyle)
    })

  }

  disconnectedCallback() {
    // console.log('disconnectedCallback', this.subscriptions);
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    });
  }

  attributeChangedCallback(attrName, oldVal, newVal) {}


  setChannel(chan) {
    this.channel = chan;
    this.channel.publish('groups-load')
    this.subscriptions.push(this.channel.subscribe('groups-changed', groups => {
      this.setData(groups);
    }))
    this.subscriptions.push(this.channel.subscribe('group-select', group => {
      // console.log('group-select',group);

      let headers=this.shadowRoot.querySelectorAll('.list-header');
      headers.forEach(h=>{
        h.classList.remove('hide');
      })

      let voteContainer= this.shadowRoot.querySelector('#voteContainer');
      //console.log('group-select',group);
      while (voteContainer.firstChild != null) {
        voteContainer.removeChild(voteContainer.firstChild);
      }
      // deputes.classList.remove('hide');


      group.nationalGroups.forEach(v => {
        let nationalGroupWC = document.createElement('national-group-wc');
        nationalGroupWC.setAttribute('mode','gauge')
        for (let style of this.propagatedStyle) {
          let injectedTemplate = document.createElement('template');
          let injectedStyle = document.createElement('style');
          injectedStyle.appendChild(document.createTextNode(style.innerText));
          injectedTemplate.appendChild(injectedStyle);
          nationalGroupWC.appendChild(injectedTemplate)
        }
        voteContainer.appendChild(nationalGroupWC);
        nationalGroupWC.setData(v);
      })

      // group.deputes.forEach(v => {
      //   let memberWC = document.createElement('member-wc');
      //   memberWC.setAttribute('mode','gauge')
      //   for (let style of this.propagatedStyle) {
      //     let injectedTemplate = document.createElement('template');
      //     let injectedStyle = document.createElement('style');
      //     injectedStyle.appendChild(document.createTextNode(style.innerText));
      //     injectedTemplate.appendChild(injectedStyle)
      //     memberWC.appendChild(injectedTemplate)
      //   }
      //   deputes.appendChild(memberWC);
      //   memberWC.setData(v);
      // })

      let laws= this.shadowRoot.querySelector('#laws');
      //console.log('group-select',group);
      while (laws.firstChild != null) {
        laws.removeChild(laws.firstChild);
      }

      group.themes.forEach(t => {

        let themeWC = document.createElement('theme-wc');
        themeWC.setAttribute('mode','gauge')
        for (let style of this.propagatedStyle) {
          let injectedTemplate = document.createElement('template');
          let injectedStyle = document.createElement('style');
          injectedStyle.appendChild(document.createTextNode(style.innerText));
          injectedTemplate.appendChild(injectedStyle)
          themeWC.appendChild(injectedTemplate)
        }

        laws.appendChild(themeWC);
        themeWC.setData(t);
      })

      let groups= this.shadowRoot.querySelector('#groups');
      groups.scrollIntoView(true);


    }));
  }

  setData(groups) {
    // console.log(groups);
    groups.forEach((group, i) => {
      let groupWC = document.createElement('group-container');
      // console.log(groupWC);
      for (let style of this.propagatedStyle) {
        let injectedTemplate = document.createElement('template');
        let injectedStyle = document.createElement('style');
        injectedStyle.appendChild(document.createTextNode(style.innerText));
        injectedTemplate.appendChild(injectedStyle)
        groupWC.appendChild(injectedTemplate)
      }
      groupWC.setChannel(this.channel);
      let groupContainer = this.shadowRoot.querySelector('#groups');
      //console.log(groupContainer);
      groupContainer.appendChild(groupWC);

      groupWC.setData(group);
    })
  }
}
window.customElements.define('partis-wc', Partis);
