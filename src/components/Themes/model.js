import vue from 'html-loader!./vue.html';

export default class Themes extends HTMLElement {
  constructor() {
    super();
    this.subscriptions = [];
    this.attachShadow({
      mode: 'open'
    });
    this.shadowRoot.innerHTML = vue;
  }
  connectedCallback() {
    this.propagatedStyle = this.querySelectorAll('style');
    this.propagatedStyle.forEach(style => {
      let injectedStyle = document.createElement('style');
      injectedStyle.appendChild(document.createTextNode(style.innerText));
      this.shadowRoot.appendChild(injectedStyle)
    })
    let urlThemes;
    let env;
    if (env == 'dev') {
      urlThemes = ''
    } else {
      urlThemes = 'http://semantic-bus.org/data/api/OPRAC-THEMES';
    }
    let themesPromise = fetch(urlThemes).then((response) => {
      return response.json();
    });

    // themesPromise.then(themes=>{
    //   console.log(themes);
    //   this.setData(themes);
    // })

  }

  disconnectedCallback() {
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    });
  }

  attributeChangedCallback(attrName, oldVal, newVal) {}


  setChannel(chan) {
    this.channel = chan;
    this.channel.publish('themes-load')
    this.subscriptions.push(this.channel.subscribe('themes-changed', themes => {
      this.setData(themes);
    }));
  }

  setData(data) {
    let main = this.shadowRoot.querySelector('#themes-container');
    for (let theme of data) {
      let themeDiv = document.createElement('div');
      themeDiv.classList.add('theme');
      themeDiv.classList.add('box');
      let icon= document.createElement('img');
      icon.classList.add('logo-theme');
      icon.setAttribute('src',theme.logo);
      themeDiv.appendChild(icon);
      let content = document.createElement('div');
      content.classList.add('content-theme');
      let lib=document.createElement('div');
      let lib2=document.createElement('div');
      lib.classList.add('lib-theme');
      lib.classList.add('containerH');
      lib2.classList.add('containerV');
      lib2.appendChild(document.createTextNode(theme.libelle));
      lib.appendChild(lib2);
      content.appendChild(lib);

      for (let law of theme.laws) {
        let lawDiv = document.createElement('a');
        lawDiv.setAttribute('href', '#/law-detail/' + law.law)
        lawDiv.classList.add('law');
        lawDiv.appendChild(document.createTextNode(law.libelle));
        content.appendChild(lawDiv);
      }
      themeDiv.appendChild(content);
      main.appendChild(themeDiv);
    }
  }
}
window.customElements.define('themes-wc', Themes);
