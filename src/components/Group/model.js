import sift from 'sift';
import vue from 'html-loader!./vue.html';
import member from '../Member/model.js';
import gauge from '../Gauge/model.js';


export default class Group extends HTMLElement {
  constructor() {
    super();
    this.subscriptions = [];
    this.attachShadow({
      mode: 'open'
    });
    this.shadowRoot.innerHTML = vue;
    this.state = 'closed';
  }
  connectedCallback() {
    this.propagatedStyle = this.querySelectorAll('style');
    this.propagatedStyle.forEach(style => {
      let injectedStyle = document.createElement('style');
      injectedStyle.appendChild(document.createTextNode(style.innerText));
      this.shadowRoot.appendChild(injectedStyle)
    })
    this.shadowRoot.querySelector('#main').addEventListener('click', e => {
      this.open();
      this.channel.publish('group-select', this.data);
    })

  }

  disconnectedCallback() {
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    });
  }

  attributeChangedCallback(attrName, oldVal, newVal) {}

  open() {
    this.shadowRoot.querySelector('#main').classList.remove('closed');
    this.classList.add('selected');
    this.channel.publish('groupe-select', this.data)
    this.state = 'opened';
  }
  close() {
    this.classList.remove('selected');
    this.shadowRoot.querySelector('#main').classList.add('closed');
    this.state = 'closed';
  }

  setChannel(channel) {
    this.channel = channel;
    this.subscriptions.push(this.channel.subscribe('group-select', group => {
      if (group.group != this.data.group) {
        this.close();
      }
    }));
  }

  setData(data) {
    this.data = data;
    // this.shadowRoot.querySelector('[name="nom"]').appendChild(document.createTextNode(data.group));
    this.shadowRoot.querySelector('[name="logo"]').setAttribute('src', data.logo);
    this.shadowRoot.querySelector('[name="evaluation"]').setData(data.evaluation);

    // this.gauge.set(data.evaluation);

    // data.votes.forEach(v => {
    //   let memberWC = document.createElement('member-wc');
    //   for (let style of this.propagatedStyle) {
    //     let injectedStyle = document.createElement('style');
    //     injectedStyle.appendChild(document.createTextNode(style.innerText));
    //     memberWC.appendChild(injectedStyle)
    //   }
    //   this.shadowRoot.querySelector('[name="voteContainer"]').appendChild(memberWC);
    //   memberWC.setData(v);
    // })
  }
}
window.customElements.define('group-container', Group);
