import vue from 'html-loader!./vue.html';

export default class Accueil extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({
      mode: 'open'
    });
    this.shadowRoot.innerHTML = vue;
  }
  connectedCallback() {
    this.propagatedStyle = this.querySelectorAll('style');
    this.propagatedStyle.forEach(style => {
      let injectedStyle = document.createElement('style');
      injectedStyle.appendChild(document.createTextNode(style.innerText));
      this.shadowRoot.appendChild(injectedStyle)
    })

  }

  disconnectedCallback() {}

  attributeChangedCallback(attrName, oldVal, newVal) {}


  setChannel(chan) {
    this.chan = chan;
  }

  setData(data) {

  }
}
window.customElements.define('accueil-wc', Accueil);
