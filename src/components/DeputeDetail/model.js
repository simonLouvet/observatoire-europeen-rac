import vue from 'html-loader!./vue.html';
import law from '../Law/model.js';
import gauge from '../Gauge/model.js';

export default class DeputeDetail extends HTMLElement {
  constructor() {
    super();
    this.subscriptions = [];
    this.attachShadow({
      mode: 'open'
    });
    this.shadowRoot.innerHTML = vue;
  }
  connectedCallback() {
    this.propagatedStyle = this.querySelectorAll('style');
    this.propagatedStyle.forEach(style => {
      let injectedStyle = document.createElement('style');
      injectedStyle.appendChild(document.createTextNode(style.innerText));
      this.shadowRoot.appendChild(injectedStyle)
    })
  }

  disconnectedCallback() {
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    });
  }

  attributeChangedCallback(attrName, oldVal, newVal) {}

  setChannel(chan, postal) {
    this.channel = chan;
    this.postal = postal;
    this.subscriptions.push(this.channel.subscribe('depute-changed', (depute) => {
      this.setData(depute);
    }));
    this.subscriptions.push(this.channel.subscribe('route-changed', (route) => {
      if (route.screen == 'depute-detail') {
        this.channel.publish('depute-load', route.id);
      }
    }));
  }

  setData(depute) {
    console.log(depute);
    this.shadowRoot.querySelector('[name="fulName"]').appendChild(document.createTextNode(depute.fullName));
    this.shadowRoot.querySelector('[name="photo"]').setAttribute('src',depute.photo);
    this.shadowRoot.querySelector('[name="nationalGroup"]').appendChild(document.createTextNode(depute.nationalGroup));
    // this.shadowRoot.querySelector('[name="groupRAC"]').appendChild(document.createTextNode(depute.groupRAC));
    if(depute.nationalGroupLogo!=null){
      this.shadowRoot.querySelector('[name="groupLogo"]').setAttribute('src',depute.nationalGroupLogo);
    }else{
      this.shadowRoot.querySelector('[name="groupLogo"]').classList.add('hide');
    }
    this.shadowRoot.querySelector('[name="evaluation"]').setData(depute.evaluation);

    let laws= this.shadowRoot.querySelector('#laws');
    //console.log('group-select',group);
    while (laws.firstChild != null) {
      laws.removeChild(laws.firstChild);
    }

    depute.laws.forEach(l => {

      let lawWC = document.createElement('law-wc');
      lawWC.setAttribute('mode','square')
      for (let style of this.propagatedStyle) {
        let injectedStyle = document.createElement('style');
        injectedStyle.appendChild(document.createTextNode(style.innerText));
        lawWC.appendChild(injectedStyle)
      }

      laws.appendChild(lawWC);
      lawWC.setData(l);
    })
  }
}
window.customElements.define('depute-detail-wc', DeputeDetail);
