import sift from 'sift';
import vue from 'html-loader!./vue.html';
import group from '../Member/model.js';

export default class Member extends HTMLElement {
  constructor() {
    super();
    this.subscriptions = [];
    const shadowRoot = this.attachShadow({
      mode: 'open'
    });
    shadowRoot.innerHTML = vue;
  }

  connectedCallback() {
    this.propagatedStyle = this.querySelectorAll('style');
    // console.log('mode', this.getAttribute('mode'));
    for (let style of this.propagatedStyle) {
      this.shadowRoot.appendChild(style)
    }
  }

  disconnectedCallback() {
    // console.log('disconnectedCallback', this.subscriptions);
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    });
  }

  attributeChangedCallback(attrName, oldVal, newVal) {
    // console.log('attributeChangedCallback', attrName);
  }

  setData(data) {
    // console.log('setData nationalGroup', data);

    this.shadowRoot.querySelector('[name="nationalGroup"]').appendChild(document.createTextNode(data.nationalGroup));
    let mode = this.getAttribute('mode');
    // console.log('setData Mode',mode);
    data.deputes.forEach(depute=>{
      let memberWC = document.createElement('member-wc');
      memberWC.setAttribute('mode',this.getAttribute('mode'))
      let voteContainer= this.shadowRoot.querySelector('#voteContainer');
      for (let style of this.propagatedStyle) {
        let injectedTemplate = document.createElement('template');
        let injectedStyle = document.createElement('style');
        injectedStyle.appendChild(document.createTextNode(style.innerText));
        injectedTemplate.appendChild(injectedStyle);
        memberWC.appendChild(injectedTemplate)
      }
      voteContainer.appendChild(memberWC);
      memberWC.setData(depute);
    })
  }
}
window.customElements.define('national-group-wc', Member);
