import vue from 'html-loader!./vue.html';
import showdown from 'showdown';
import gauge from '../Gauge/model.js';

export default class Gouvernement extends HTMLElement {
  constructor() {
    super();
    this.subscriptions = [];
    this.attachShadow({
      mode: 'open'
    });
    this.shadowRoot.innerHTML = vue;
  }
  connectedCallback() {
    this.propagatedStyle = this.querySelectorAll('style');
    this.propagatedStyle.forEach(style => {
      let injectedStyle = document.createElement('style');
      injectedStyle.appendChild(document.createTextNode(style.innerText));
      this.shadowRoot.appendChild(injectedStyle)
    })

  }

  disconnectedCallback() {
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    });
  }

  attributeChangedCallback(attrName, oldVal, newVal) {}


  setChannel(channel) {
    this.channel = channel;
    channel.publish('gouvernement-load');
    this.subscriptions.push(this.channel.subscribe('gouvernement-changed', gouvernement => {
      //console.log('gouvernement',gouvernement);
      let main = this.shadowRoot.querySelector('#laws');
      let globalEvaluation = 0;
      for (let g of gouvernement) {
        let lawDiv = document.createElement('div');
        lawDiv.classList.add('box');
        lawDiv.classList.add('law');
        lawDiv.addEventListener('click', e => {
          main.querySelectorAll('.law').forEach(l => {
            l.querySelector('.lawText').classList.add('hide');
            l.querySelector('.lawHeader').classList.remove('selected');
          });
          lawDiv.querySelector('.lawText').classList.remove('hide');
          lawDiv.querySelector('.lawHeader').classList.add('selected');
        })

        let header = document.createElement('div');
        header.classList.add('containerH');
        header.classList.add('pointer');
        header.classList.add('lawHeader');
        let lawNameDiv = document.createElement('div');
        lawNameDiv.classList.add('lawName');
        lawNameDiv.classList.add('containerV');
        lawNameDiv.classList.add('container-content-centered');
        // lawNameDiv.classList.add('blue-dark');
        let lawNameDivText = document.createElement('div');
        lawNameDivText.appendChild(document.createTextNode(g.libelle))
        lawNameDiv.appendChild(lawNameDivText);
        header.appendChild(lawNameDiv)

        let lawEvaluation = document.createElement('gauge-wc');
        lawEvaluation.classList.add('lawEvaluation');

        header.appendChild(lawEvaluation)

        lawDiv.appendChild(header);



        var converter = new showdown.Converter({ openLinksInNewWindow: true });
        var html = converter.makeHtml(g.md);
        let lawTextDiv = document.createElement('div');
        lawTextDiv.classList.add('lawText');
        lawTextDiv.classList.add('hide');
        lawTextDiv.insertAdjacentHTML('afterbegin', html);
        lawDiv.appendChild(lawTextDiv);
        main.appendChild(lawDiv);

        lawEvaluation.setData(g.evaluation);
        globalEvaluation += g.evaluation;
      }
      globalEvaluation = globalEvaluation / gouvernement.length;
      this.shadowRoot.querySelector('#globalEval').setData(globalEvaluation);
    }));


  }

  setData(data) {

  }
}
window.customElements.define('gouvernement-wc', Gouvernement);
