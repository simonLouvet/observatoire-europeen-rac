import sift from 'sift';
import vue from 'html-loader!./vue.html';

export default class Law extends HTMLElement {
  constructor() {
    super();
    this.subscriptions = [];
    const shadowRoot = this.attachShadow({
      mode: 'open'
    });
    shadowRoot.innerHTML = vue;
  }




  connectedCallback() {
    this.propagatedStyle = this.querySelectorAll('style');
    // console.log('mode', this.getAttribute('mode'));
    for (let style of this.propagatedStyle) {
      this.shadowRoot.appendChild(style)
    }
  }

  disconnectedCallback() {
    // console.log('disconnectedCallback', this.subscriptions);
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    });
  }

  attributeChangedCallback(attrName, oldVal, newVal) {
    // console.log('attributeChangedCallback', attrName);
  }

  setData(data) {
    // console.log('setData', data);
    this.shadowRoot.querySelector('[name="law"]').appendChild(document.createTextNode(data.libelle));
    let absent = true;
    for  (let lawKey in data.votes){
      let law=data.votes[lawKey];
      for  (let voteKey in law.votes){
        if (law.votes[voteKey]!="absent"){
          absent=false;
        }
      }
    }
    if (absent==true){
      this.shadowRoot.querySelector('[name="law"]').appendChild(document.createTextNode(" (absent.e)"));
    }
    this.shadowRoot.querySelector('#main').setAttribute('href','#/law-detail/'+data.law)
    let mode = this.getAttribute('mode');
    // console.log('setData Mode',mode);
    if (mode == 'square') {
      let csslight;
      if (data.evaluation < 0.33) {
        csslight = 'red-spot';
      } else if (data.evaluation < 0.66) {
        csslight = 'orange-spot';
      } else {
        csslight = 'green-spot';
      }
      this.shadowRoot.querySelector('[name="evaluation-square"]').classList.add(csslight);
      this.shadowRoot.querySelector('[name="evaluation-square"]').classList.remove('hide');
      this.shadowRoot.querySelector('[name="evaluation-gauge"]').classList.add('hide');
    } else if (mode == 'gauge') {
      this.shadowRoot.querySelector('[name="evaluation-gauge"]').setData(data.evaluation);
      this.shadowRoot.querySelector('[name="evaluation-gauge"]').classList.remove('hide');
      this.shadowRoot.querySelector('[name="evaluation-square"]').classList.add('hide');
    }


    // this.shadowRoot.querySelector('[name="evaluation"]').appendChild(document.createTextNode(data.fullName));
  }
}
window.customElements.define('law-wc', Law);
