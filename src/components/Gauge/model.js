import vue from 'html-loader!./vue.html';
import Gauge from 'gaugeJS';

export default class GaugeWC extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({
      mode: 'open'
    });
    this.shadowRoot.innerHTML = vue;
  }
  connectedCallback() {
    this.propagatedStyle = this.querySelectorAll('style');
    this.propagatedStyle.forEach(style => {
      let injectedStyle = document.createElement('style');
      injectedStyle.appendChild(document.createTextNode(style.innerText));
      this.shadowRoot.appendChild(injectedStyle)
    })

    var opts = {
      angle: 0.15, /// The span of the gauge arc
      lineWidth: 0.44, // The line thickness
      pointer: {
        length: 0.9, // Relative to gauge radius
        strokeWidth: 0.035 // The thickness
      },
      colorStart: '#6FADCF', // Colors
      colorStop: '#8FC0DA', // just experiment with them
      strokeColor: '#E0E0E0', // to see which ones work best for you
      staticZones: [
           {strokeStyle: "#F03E3E", min: 0, max: 0.33}, // Green
           {strokeStyle: "#FFDD00", min: 0.33, max: 0.66}, // Yellow
           {strokeStyle: "#30B32D", min: 0.66, max: 1}  // Red
        ]
    };
    var target = this.shadowRoot.querySelector('#gauge'); // your canvas element
    var gauge =  new Gauge.Gauge(target); // create sexy gauge!
    gauge.setOptions(opts); // create sexy gauge!
    gauge.minValue =0;  // Prefer setter over gauge.minValue = 0
    gauge.maxValue = 1; // set max gauge value
    this.gauge=gauge;

  }

  disconnectedCallback() {}

  attributeChangedCallback(attrName, oldVal, newVal) {}


  setChannel(chan) {
    this.chan = chan;
  }

  setData(evaluation) {
    this.evaluation = evaluation;
    this.gauge.set(evaluation);
  }

}
window.customElements.define('gauge-wc', GaugeWC);
