import Navigo from 'navigo';

export default class MainModel {
  constructor(config) {
    try {
      this.subscriptions = [];
      console.log(config);

      let dataPath = config.dataPath;
      let dataSuffix = config.dataSuffix;

      let urlLaws = dataPath + 'OPRAC-LAWS' + dataSuffix;

      console.log(urlLaws);


      this.lawsPromise = fetch(urlLaws).then((response) => {
        return response.json();
      });
      let urlThemes = dataPath + 'OPRAC-THEMES' + dataSuffix;
      this.themesPromise = fetch(urlThemes).then((response) => {
        return response.json();
      });
      let urlVotes = dataPath + 'OPRAC-BASE' + dataSuffix;
      this.votesPromise = fetch(urlVotes).then((response) => {
        return response.json();
      });
      let urlGroupes = dataPath + 'OPRAC-GROUPES'+ dataSuffix;
      this.groupsPromise = fetch(urlGroupes).then((response) => {
        return response.json();
      });
      let urlGouvernement = dataPath + 'OPRAC-GOUVERNEMENT' + dataSuffix;
      this.gouvernementPromise = fetch(urlGouvernement).then((response) => {
        return response.json();
      });
    } catch (e) {
      console.log(e);
    } finally {

    }

    // let urlLaws;
    // if (env == 'dev') {
    //   urlLaws = './data/'
    // } else {
    //   urlLaws = 'https://semantic-bus.org/data/api/OPRAC-LAWS';
    // }
    // this.lawsPromise = fetch(urlLaws).then((response) => {
    //   return response.json();
    // });
    //
    //
    // let urlThemes;
    // if (env == 'dev') {
    //   urlThemes = './data/'
    // } else {
    //   urlThemes = 'https://semantic-bus.org/data/api/OPRAC-THEMES';
    // }
    // this.themesPromise = fetch(urlThemes).then((response) => {
    //   return response.json();
    // });
    //
    // let urlVotes;
    // if (env == 'dev') {
    //   urlVotes = './data/'
    // } else {
    //   urlVotes = 'https://semantic-bus.org/data/api/OPRAC-BASE';
    // }
    // this.votesPromise = fetch(urlVotes).then((response) => {
    //   return response.json();
    // });
    //
    // let urlGroupes;
    // if (env == 'dev') {
    //   urlGroupes = './data/OPRAC-groupes.json'
    // } else {
    //   urlGroupes = 'https://semantic-bus.org/data/api/OPRAC-GROUPES';
    // }
    // this.groupsPromise = fetch(urlGroupes).then((response) => {
    //   return response.json();
    // });
    //
    // let urlGouvernement;
    // if (env == 'dev') {
    //   urlGouvernement = './data/'
    // } else {
    //   urlGouvernement = 'https://semantic-bus.org/data/api/OPRAC-GOUVERNEMENT';
    //
    // }
    // this.gouvernementPromise = fetch(urlGouvernement).then((response) => {
    //   return response.json();
    // });

  }


  getThemes() {
    return new Promise((resolve, reject) => {
      if (this.themes != undefined) {
        resolve(this.deepCopy(this.themes));
      } else {
        this.themesPromise.then(themes => {
          resolve(themes);
          this.themes = this.deepCopy(themes);
        })
      }
    })
  }

  getVotes() {
    return new Promise((resolve, reject) => {
      if (this.votes != undefined) {
        resolve(this.deepCopy(this.votes));
      } else {
        this.votesPromise.then(votes => {
          resolve(votes);
          this.votes = this.deepCopy(votes);
        })
      }
    })
  }

  getLaws() {
    return new Promise((resolve, reject) => {
      if (this.laws != undefined) {
        resolve(this.deepCopy(this.laws));
      } else {
        this.lawsPromise.then(async laws => {
          let MDPomises = laws.map(l => {
            return new Promise((resolve, reject) => {
              fetch('https://gitlab.com/api/v4/projects/9568353/wikis/' + l.doc).then((response) => {
                return response.json();
              }).then(docs => {
                resolve(docs);
              });
            });
          });
          //console.log('MDPomises',MDPomises);
          let mds = await Promise.all(MDPomises)
          laws.forEach((law, i) => {
            law.md = mds[i].content;
          })
          resolve(this.deepCopy(laws));
          this.laws = laws;
        })
      }
    })
  }

  getGroups() {
    return new Promise((resolve, reject) => {
      if (this.groups != undefined) {
        resolve(this.deepCopy(this.groups));
      } else {
        this.groupsPromise.then(groups => {
          resolve(this.deepCopy(groups));
          this.groups = groups;
        })
      }
    })
  }

  getGouvernement() {
    return new Promise((resolve, reject) => {
      if (this.gouvernement != undefined) {
        resolve(this.deepCopy(this.gouvernement));
      } else {
        this.gouvernementPromise.then(async gouvernement => {


          let MDPomises = gouvernement.map(g => {
            return new Promise((resolve, reject) => {
              fetch('https://gitlab.com/api/v4/projects/9568353/wikis/' + g.doc).then((response) => {
                return response.json();
              }).then(docs => {
                resolve(docs);
              });
            });
          });
          //console.log('MDPomises',MDPomises);
          let mds = await Promise.all(MDPomises)
          gouvernement.forEach((g, i) => {
            g.md = mds[i].content;
          })
          resolve(this.deepCopy(gouvernement));
          this.gouvernement = gouvernement;
        })
      }
    })
  }

  publishThemes() {
    this.getThemes().then(themes => {
      this.publish('themes-changed', themes);
    })
  }

  publishGouvernement() {
    this.getGouvernement().then(themes => {
      this.publish('gouvernement-changed', themes);
    })
  }


  publishGroups() {
    let startTime = new Date();
    this.getGroups().then(async groups => {

      let votes = await this.getVotes();
      // console.log('votes', votes);
      groups.forEach(async (group, i) => {
        group.votes = votes.filter(v => v.groupRAC == group.group);
        if (group.votes.length > 0) {
          let totalGroup = group.votes.reduce((accumulator, currentValue) => accumulator + currentValue.evaluation, 0);
          group.evaluation = totalGroup / group.votes.length;
        } else {
          group.evaluation = 0;
        }
        group.themes = this.groupVotesByTheme(group.votes);

        // let laws = await this.getLaws();
        // group.laws = JSON.parse(JSON.stringify(laws));
        // group.laws.forEach(l => {
        //   l.votes = group.votes.filter(v => v.law == l.law);
        //   l.evaluation = this.computeEvaluation(l.votes);
        //   // console.log('l',l);
        //   // l.deputes = this.groupVotesByDepute(l.votes);
        //
        // })
        group.nationalGroups = this.groupVotesByNationalGroup(group.votes);
        group.nationalGroups.forEach(n => {
          n.deputes.sort((a, b) => {
            let diff;
            if (b.evaluation == a.evaluation) {
              diff = a.fullName.localeCompare(b.fullName);
            } else {
              diff = b.evaluation - a.evaluation;
            }
            return diff;
          });
        })

        // group.deputes.sort((a, b) => {
        //     // console.log(b.fullName,a.fullName,b.fullName.localeCompare(a.fullName));
        //     return a.fullName.localeCompare(b.fullName);
        // });
      });
      groups.sort((a, b) => {
        let diff;
        if (b.evaluation == a.evaluation) {
          diff = a.group.localeCompare(b.group);
        } else {
          diff = b.evaluation - a.evaluation;
        }
        return diff;
      });
      // console.log('publishGroups Time',new Date()-startTime);
      this.publish('groups-changed', groups);
    })
  }

  getLaw(law) {
    let startTime = new Date();
    this.getLaws().then(async laws => {
      // console.log(laws);
      let votes = await this.getVotes();
      laws.forEach((law, i) => {
        law.votes = votes.filter(v => v.law == law.law);
      })
      // console.log('getLaw Time0',new Date()-startTime);

      let groups = await this.getGroups();
      // console.log('getLaw Time1',new Date()-startTime);

      laws.forEach((law, i) => {
        // law.groups = groups;
        law.groups = JSON.parse(JSON.stringify(groups));
        // console.log('getLaw Time2',new Date()-startTime);
        law.groups.forEach((group) => {
          //console.log(group.group,law.votes,law.votes.filter(v => v.groupRAC == group.group));
          group.votes = law.votes.filter(v => v.groupRAC == group.group);
          group.evaluation = this.computeEvaluation(group.votes);
          group.nationalGroups = this.groupVotesByNationalGroup(group.votes);
        })
        // console.log('getLaw Time3',new Date()-startTime);
        law.groups.sort((a, b) => b.evaluation - a.evaluation);
      })

      let out = laws.filter(l => l.law == law)[0];
      this.publish('law-changed', out);
      // console.log('getLaw Time',new Date()-startTime);
    })
  }

  getDepute(depute) {
    let startTime = new Date();
    this.getVotes().then(async votes => {
      let deputeVote = votes.filter(v => v.fullName == depute);
      let out = this.groupVotesByDepute(deputeVote)[0];
      out.laws = [];
      let allLaws = await this.getLaws();
      out.votes.forEach(v => {
        let law = allLaws.filter(l => l.law == v.law)[0];
        law.votes = deputeVote.filter(d => d.law == v.law)
        law.evaluation = v.evaluation;
        out.laws.push(law);
      });
      // console.log('getDepute Time', new Date() - startTime);
      this.publish('depute-changed', out);
    })
  }

  setChannel(chan) {
    this.channel = chan;
    this.channel.subscribe('themes-load', () => {
      this.publishThemes();
    });
    this.channel.subscribe('groups-load', () => {
      this.publishGroups();
    });
    this.channel.subscribe('law-load', (law) => {
      this.getLaw(law);
    });
    this.channel.subscribe('depute-load', (depute) => {
      this.getDepute(depute);
    });
    this.channel.subscribe('gouvernement-load', (depute) => {
      this.publishGouvernement();
    });
  }

  publish(message, data) {
    let count = 0;
    let checkExist = setInterval(() => {
      if (this.channel != undefined) {
        //console.log('CRUD message',message);
        clearInterval(checkExist);
        // console.log('publish',message);
        this.channel.publish(message, data)
      } else {
        count++;
        if (count > 100) {
          clearInterval(checkExist);
          console.warn(`http channel doesn't exist after 10s`);
        }
      }
    }, 100); // check every 100ms
  }

  computeEvaluation(votes) {
    let evaluation;
    if (votes.length > 0) {
      let total = votes.reduce((accumulator, currentValue) => accumulator + currentValue.evaluation, 0);
      evaluation = total / votes.length;
    } else {
      evaluation = 0;
    }
    return evaluation;
  }

  groupVotesByTheme(votes) {
    let grouped = votes.reduce((accumulator, current) => {
      let theme;
      theme = accumulator.filter(r => r.thematique == current.thematique)[0];
      if (theme != undefined) {
        theme.votes.push(current);
      } else {
        theme = {
          thematique: current.thematique
        };
        theme.votes = [current];
        accumulator.push(theme);
      }
      return accumulator
    }, []);
    grouped.forEach(theme => {
      theme.laws = this.groupVotesByLaw(theme.votes);
      theme.evaluation = this.computeEvaluation(theme.votes);
    })

    // console.log('groupVotesByTheme',grouped);
    return grouped;
  }

  groupVotesByLaw(votes) {
    let grouped = votes.reduce((accumulator, current) => {
      let law;
      law = accumulator.filter(r => r.law == current.law)[0];
      if (law != undefined) {
        law.votes.push(current);
      } else {
        law = {
          law: current.law,
          libelle: current.lawLibelle
        };
        law.votes = [current];
        accumulator.push(law);
      }
      return accumulator
    }, []);
    grouped.forEach(law => {
      law.evaluation = this.computeEvaluation(law.votes);
    })
    // console.log('groupVotesByLaw',grouped);
    return grouped;
  }

  groupVotesByNationalGroup(votes) {
    let grouped = votes.reduce((accumulator, current) => {
      let nationalGroup;
      nationalGroup = accumulator.filter(r => r.nationalGroup == current.nationalGroup)[0];
      if (nationalGroup != undefined) {
        nationalGroup.votes.push(current);
      } else {
        nationalGroup = {
          nationalGroup: current.nationalGroup,
          nationalGroupLogo: current.nationalGroupLogo
        };
        nationalGroup.votes = [current];
        accumulator.push(nationalGroup);
      }
      return accumulator
    }, []);
    grouped.forEach(nationalGroup => {
      nationalGroup.deputes = this.groupVotesByDepute(nationalGroup.votes);
      nationalGroup.evaluation = this.computeEvaluation(nationalGroup.votes);
    })
    return grouped;
  }


  groupVotesByDepute(votes) {
    let grouped = votes.reduce((accumulator, current) => {
      let member;
      member = accumulator.filter(r => r.fullName == current.fullName)[0];
      if (member != undefined) {
        member.votes.push(current);
      } else {
        member = {
          fullName: current.fullName,
          photo: current.photo,
          nationalGroup: current.nationalGroup,
          groupRAC: current.groupRAC,
          groupLogo: current.groupLogo,
          nationalGroupLogo: current.nationalGroupLogo
        };
        member.votes = [current];
        accumulator.push(member);
      }
      // }
      return accumulator
    }, []);
    grouped.forEach(depute => {
      depute.evaluation = this.computeEvaluation(depute.votes);
    })
    return grouped;
  }

  deepCopy(obj) {
    return JSON.parse(JSON.stringify(obj));
  }

}
