import sift from 'sift';
import showdown from 'showdown';
import postal from 'postal';
import group from './components/Group/model.js';
import accueil from './components/Accueil/model.js';
import gouvernement from './components/Gouvernement/model.js';
import partis from './components/Partis/model.js';
import themes from './components/Themes/model.js';
import lawDetail from './components/LawDetail/model.js';
import deputeDetail from './components/DeputeDetail/model.js';
import Router from './control/Router.js';
import MainModel from './models/MainModel.js';
import cssContainers from './styles/containers.css';
import cssShared from './styles/shared.css';
import cssColors from './styles/colors.css';

let loadComponent = function(comp) {
  // let comp= route.screen+'-wc';
  //console.log('create element',comp);
  //  console.log(navigator.getAttribute('component-navigation'));
  let main = document.querySelector('#main');
  let component = document.createElement(comp);

  let injectedTemplate = document.createElement('template');
  let injectedStyle = document.createElement('style');
  injectedTemplate.appendChild(injectedStyle)

  injectedStyle.appendChild(document.createTextNode(cssContainers.toString()));
  injectedStyle.appendChild(document.createTextNode(cssShared.toString()));
  injectedStyle.appendChild(document.createTextNode(cssColors.toString()));
  component.appendChild(injectedTemplate);
  // console.log(main);
  while (main.firstChild != null) {
    main.removeChild(main.firstChild);
  }

  main.appendChild(component);
  return component;
}

let start = async function(env) {

  fetch('./data/config.json').then(response=>{
    return response.json();
  }).then(config=>{
    let mainChannel = postal.channel('main');

    let router = new Router();
    router.setChannel(mainChannel);

    let mainModel = new MainModel(config);
    mainModel.setChannel(mainChannel);

    mainChannel.subscribe('route', route => {
      // console.log('ROUTE in MAIN',route);
      let menus=document.querySelectorAll('a');
      menus.forEach(m=>{
        // console.log(m.getAttribute('href'),route,m.getAttribute('href').indexOf(route));
        if(m.getAttribute('href').indexOf(route.screen)!=-1){
          m.classList.add('selected');
        }else {
          m.classList.remove('selected');
        }
      })
      let comp = route.screen + '-wc';
      let component = loadComponent(comp);
      component.setChannel(mainChannel,postal);
      mainChannel.publish('route-changed', route);
    })
  });
}
window.addEventListener('load', () => {
  start();
})
// console.log(start);
